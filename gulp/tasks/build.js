var gulp = require('gulp'),
imagemin = require('gulp-imagemin'),
del = require('del'),
usemin = require('gulp-usemin'),
rev = require('gulp-rev'),
cssnano = require('gulp-cssnano'),
uglify = require('gulp-uglify'),
babel = require('gulp-babel'),
browserSync = require('browser-sync').create();

gulp.task("previewDist", function() {
    browserSync.init({
        notify: false,
        server: {
          baseDir: "public"
        }
    });
});

// Delete Dist folder to start the build task clean.
gulp.task("deleteDistFolder", function() {
    return del("./public")
});

// Copy general files
gulp.task('copyGeneralFiles', ["deleteDistFolder"], function() {
    var paths = [
        "./app/**/*",
        "!./app/index.html",
        "!./app/assets/images/**",
        "!./app/assets/styles/**",
        "!./app/assets/scripts/**",
        "!./app/temp/",
        "!./app/temp/**"
    ];

    return gulp.src(paths)
        .pipe(gulp.dest("./public"));
});

// Optimize Images
gulp.task("optimize-images", ["deleteDistFolder"], function() {
    return gulp.src(["./app/assets/images/**/*", "!./app/assets/images/icons/", "!./app/assets/images/icons/**/*"])
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
            multipass: true
        }))
        .pipe(gulp.dest("./public/assets/images"));
});

gulp.task("usemin", ["deleteDistFolder"], function() {
    return gulp.src("./app/index.html")
        .pipe(usemin({
            css: [function() {return rev()}, function() {return cssnano()}],
            js: [function() {return babel({presets: ["es2015"]})}, function() {return rev()}, function() {return uglify()}]
        }))
        .pipe(gulp.dest("./public/"));
});

// Shortcut to all tasks.
gulp.task("build", [
    // tasks as dependencies
    "deleteDistFolder",
    "copyGeneralFiles",
    "optimize-images",
    "usemin"
]);